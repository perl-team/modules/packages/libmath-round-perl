Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Math-Round
Upstream-Contact: Neil Bowers <neilb@cpan.org>
Source: https://metacpan.org/release/Math-Round

Files: *
Copyright: 2000, Geoffrey Rommel <GROMMEL@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2001-2002, Matt Hope <dopey@debian.org>
 2006-2007, Carlo Segre <segre@debian.org>
 2007-2023, gregor herrmann <gregoa@debian.org>
 2013-2015, Axel Beckert <abe@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the
 General Public License can be found in `/usr/share/common-licenses/GPL-1'.
